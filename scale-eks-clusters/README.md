### This python script can be used to scale in and out all the node groups of multiple EKS clusters at once

##### Usage
```sh
usage: scale-eks-clusters.py [-h] [--scale-out] [--scale-in] [--config-file CONFIG_FILE]

Command line utility to scale out and scale in EKS cluster node groups

options:
  -h, --help            show this help message and exit
  --scale-out           pass this flag to scale out the node group
  --scale-in            pass this flag to scale in the node group
  --config-file         path to config file
```

##### Schema of config file
```sh
{
    "list_of_clusters": {
        "cluster-1": {                    ## This is the name of the cluster
            "scale_in_capacity": {
                "min_count": 0,           ## This will set min, desired and max counts to 0 if --scale-in flag is passed
                "desired_count": 0,
                "max_count": 0
            },
            "scale_out_capacity": {       ## This will set min, desired and max counts to the following values if --scale-out flag is passed
                "min_count": 1,
                "desired_count": 2,
                "max_count": 3
            }
        },
        "cluster-2": {                    ## This is the name of the cluster
            "scale_in_capacity": {
                "min_count": 2,
                "desired_count": 2,
                "max_count": 2
            },
            "scale_out_capacity": {
                "min_count": 10,
                "desired_count": 10,
                "max_count": 10
            }
        },
        .
        .
        .
        .
        .
    }
}
```

##### Examples
- To scale in clusters, this expects the config file at the same PWD as the python code and with the name config.json
```sh
python3 scale-eks-clusters.py --scale-in
```
- To scale out clusters, this expects the config file at the same PWD as the python code and with the name config.json
```sh
python3 scale-eks-clusters.py --scale-out
```
- To scale in clusters
```sh
python3 scale-eks-clusters.py --scale-in --config-file ./path/to/config/file.json
```
- To scale out clusters
```sh
python3 scale-eks-clusters.py --scale-out --config-file ./path/to/config/file.json
```

##### Notes:
- This code expects any of the AWS Authentication method in place before execution and obeys the default precendence behaviour of AWS SDK (https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html#cli-configure-quickstart-precedence)

